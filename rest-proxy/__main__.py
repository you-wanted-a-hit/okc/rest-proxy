from flask import Flask, request, jsonify, make_response
from kafka import KafkaConsumer, KafkaProducer
from decouple import config
import logging
import json

def get_logger(name, level=logging.DEBUG):
    formatter = logging.Formatter(fmt='[ %(asctime)s ] %(levelname)-4s %(name)-4s {%(module)s:%(funcName)s:%(lineno)s} %(message)s')

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger

class Consumer:
    def __init__(self, host, port, topics):
        self.consumer = KafkaConsumer(
            topics,
            bootstrap_servers=[f'{host}:{port}'],
            auto_offset_reset='earliest',
            # enable_auto_commit=True,
            # auto_commit_interval_ms=1000,
            max_poll_records=1,
            max_poll_interval_ms=1000,
            group_id=config("KAFKA_CONSUMER_GROUP"),
            value_deserializer=lambda msg: json.loads(msg.decode('utf-8')),
            key_deserializer=lambda key: key.decode('utf-8'))

    def get_consumer(self):
        return self.consumer

    def poll_one(self):
        for message in self.consumer:
            try:
                self.consumer.commit()
            except Exception:
                pass
            return message.value

class Producer:
    def __init__(self, host, port):
        self.producer = KafkaProducer(
            bootstrap_servers=[f'{host}:{port}'],
            value_serializer= lambda msg: json.dumps(msg).encode('utf-8'),
            key_serializer= lambda key: key.encode('utf-8'))
        
    def publish(self, key, message, topic):
        self.producer.send(topic, message, key=key)

host=config("KAFKA_HOST_NAME")
port=config("KAFKA_HOST_PORT")
consumer = Consumer(host=host, port=port, topics=config("KAFKA_CONSUMER_TOPIC"))
producer = Producer(host=host, port=port)
topic=config("KAFKA_PRODUCER_TOPIC")
logger= get_logger('REST-PROXY')

app = Flask(__name__)

@app.route(config('SECRET_URI'), methods=['POST', 'OPTIONS'])
def greeting():
    if request.method == 'OPTIONS': 
        return build_preflight_response()
    elif request.method == 'POST': 
        req = request.get_json()
        logger.debug(req)
        logger.info(f'Forwarding message {req}')
        producer.publish(key='api', message=req, topic=topic)
        logger.info('Waiting for response...')
        result = consumer.poll_one()
        response = make_response(jsonify(result))
        logger.debug(f'RES -> {response}')
        return build_actual_response(response)

def build_preflight_response():
    response = make_response()
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add('Access-Control-Allow-Headers', "*")
    response.headers.add('Access-Control-Allow-Methods', "*")
    return response

def build_actual_response(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000, debug=True)

